package com.eve.rewardz.view.login.presentation

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.eve.rewardz.core.utils.SharedPrefs
import com.eve.rewardz.databinding.ActivityLoginBinding
import com.eve.rewardz.view.login.presentation.viewmodel.LoginActivityState
import com.eve.rewardz.view.login.presentation.viewmodel.LoginViewModel
import com.eve.rewardz.view.main.MainActivity
import com.eve.rewardz.view.register.presentation.RegisterActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding

    private val loginViewModel: LoginViewModel by viewModels()

    @Inject
    lateinit var sharedPrefs: SharedPrefs

    private var back_pressed: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)


        observe()
        initView()
    }

    fun initView(){
        binding.btnLogin.setOnClickListener {
            when(loginViewModel.isInputValid(binding))  {
                true -> {
                    val strPhoneNumber = binding.etPhone.text.toString()
                    loginViewModel.users(strPhoneNumber)
                }
                false -> {
                    Toast.makeText(this, "Field cannot empty", Toast.LENGTH_SHORT).show()
                }
            }
        }

        binding.tvRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
    }

    private fun observe() {
        observeState()
        observeLogin()
    }

    private fun observeState() {
        loginViewModel.state.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { state -> handleState(state) }
            .launchIn(lifecycleScope)
    }

    private fun observeLogin() {
        loginViewModel.users.flowWithLifecycle(
            lifecycle,
            Lifecycle.State.STARTED
        )
            .onEach { detail ->
                detail?.let { handleLogin(detail[0].phoneNumber) }
            }
            .launchIn(lifecycleScope)
    }

    private fun handleState(state: LoginActivityState) {
        when (state) {
            is LoginActivityState.Success -> {
                Toast.makeText(this, state.message, Toast.LENGTH_SHORT).show()
            }
            is LoginActivityState.Init -> {

            }
            is LoginActivityState.ShowToast -> {

            }
            is LoginActivityState.IsLoading -> {

            }
            is LoginActivityState.Error -> {
                Toast.makeText(this, state.msg, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun handleLogin(phoneNumber: String) {
        sharedPrefs.saveLoginPref(phoneNumber)
        startActivity(Intent(this, MainActivity::class.java))
    }

    override fun onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis()) {
            moveTaskToBack(true)
            finish()
        } else {
            Toast.makeText(this, "Press once again to exit", Toast.LENGTH_SHORT).show()
        }
        back_pressed = System.currentTimeMillis()
    }

    companion object {
        private const val TAG = "LoginActivity"
    }
}