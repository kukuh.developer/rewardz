package com.eve.rewardz.view.card.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eve.rewardz.R
import com.eve.rewardz.core.database.card.Card
import com.eve.rewardz.databinding.ItemCardBinding

class CardAdapter (var context: Context,
                   var items: List<Card>
) : RecyclerView.Adapter<CardAdapter.ItemViewHolder>() {

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val binding = ItemCardBinding.bind(itemView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_card, parent, false)
        return ItemViewHolder(view)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val data = items[position]
        with(holder) {
            binding.tvCardHolderName.text = data.cardName
            binding.tvCardNumber.text = data.cardNumber
        }
    }
}