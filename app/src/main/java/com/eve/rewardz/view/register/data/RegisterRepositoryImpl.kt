package com.eve.rewardz.view.register.data

import com.eve.rewardz.core.database.user.User
import com.eve.rewardz.core.database.user.UserDao
import com.eve.rewardz.view.register.domain.RegisterRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class RegisterRepositoryImpl @Inject constructor(private val userDao: UserDao) : RegisterRepository {
    override suspend fun user(user: User): Flow<Boolean> {
        return flow {
            try {
                userDao.insertUser(user)
                emit(true)
            } catch (e: Exception) {
                emit(false)
            }
        }
    }
}