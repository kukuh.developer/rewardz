package com.eve.rewardz.view.home.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.eve.rewardz.core.database.card.Card
import com.eve.rewardz.core.utils.HorizontalItemDecoration
import com.eve.rewardz.core.utils.SharedPrefs
import com.eve.rewardz.databinding.FragmentHomeBinding
import com.eve.rewardz.view.home.presentation.adapter.HomeCardAdapter
import com.eve.rewardz.view.home.presentation.viewmodel.HomeState
import com.eve.rewardz.view.home.presentation.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var binding: FragmentHomeBinding
    
    private lateinit var homeCardAdapter: HomeCardAdapter
    
    private val homeViewModel: HomeViewModel by viewModels()

    @Inject
    lateinit var sharedPrefs: SharedPrefs

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        observe()
        initView()
        return binding.root
    }

    private fun initView(){
        initRecyclerView()
        cards()
    }

    private fun observe() {
        observeState()
        observeCards()
    }

    fun cards(){
        homeViewModel.cards(sharedPrefs.getuserPhoneNumber())
    }

    private fun initRecyclerView() {
        homeCardAdapter = HomeCardAdapter(requireContext(), listOf())
        val itemDecoration = HorizontalItemDecoration(0, 10)
        if(binding.rvCards.itemDecorationCount == 0){
            binding.rvCards.addItemDecoration(itemDecoration)
        }
        binding.rvCards.adapter = homeCardAdapter
    }

    private fun observeState() {
        homeViewModel.state.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { state -> handleState(state) }
            .launchIn(lifecycleScope)
    }

    private fun observeCards() {
        homeViewModel.cards.flowWithLifecycle(
            lifecycle,
            Lifecycle.State.STARTED
        )
            .onEach { detail ->
                detail?.let { handleCards(it) }
            }
            .launchIn(lifecycleScope)
    }

    private fun handleState(state: HomeState) {
        when (state) {
            is HomeState.Success -> {
                //Toast.makeText(requireContext(), state.message, Toast.LENGTH_SHORT).show()
            }
            is HomeState.Init -> {

            }
            is HomeState.ShowToast -> {

            }
            is HomeState.IsLoading -> {

            }
            is HomeState.Error -> {
                //Toast.makeText(requireContext(), state.msg, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun handleCards(cards: MutableList<Card>) {
        homeCardAdapter.items = cards
        homeCardAdapter.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        cards()
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}