package com.eve.rewardz.view.card.data

import com.eve.rewardz.core.database.card.Card
import com.eve.rewardz.core.database.card.CardDao
import com.eve.rewardz.view.card.domain.CardRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class CardRepositoryImpl @Inject constructor(private val cardDao: CardDao) : CardRepository {
    override suspend fun cards(userPhone: String): Flow<MutableList<Card>> {
        return flow {
            val response = cardDao.getCards(userPhone)
            if(response!!.size>0){
                emit(response)
            } else {
                emit(mutableListOf())
            }
        }
    }

    override suspend fun addCard(card: Card): Flow<Boolean> {
        return flow {
            try {
                cardDao.insertCard(card)
                emit(true)
            } catch (e: Exception) {
                emit(false)
            }
        }
    }

    companion object {
        private const val TAG = "CardRepositoryImpl"
    }
}