package com.eve.rewardz.view.home.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.eve.rewardz.core.database.card.Card
import com.eve.rewardz.view.home.domain.HomeUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val homeUseCase: HomeUseCase): ViewModel() {
    val state = MutableStateFlow<HomeState>(HomeState.Init)
    val mState: StateFlow<HomeState> get() = state

    private val _cards = MutableStateFlow<MutableList<Card>?>(null)
    val cards : StateFlow<MutableList<Card>?> get() = _cards

    private fun setLoading(){
        state.value = HomeState.IsLoading(true)
    }

    private fun hideLoading(){
        state.value = HomeState.IsLoading(false)
    }

    private fun showToast(message: String){
        state.value = HomeState.ShowToast(message)
    }

    fun cards(userPhone: String){
        viewModelScope.launch {
            homeUseCase.cards(userPhone)
                .onStart {
                    setLoading()
                }
                .catch { exception ->
                    hideLoading()
                    showToast(exception.message.toString())
                }
                .collect { result ->
                    hideLoading()
                    if(result.isNotEmpty()){
                        state.value = HomeState.Success("Success")
                        _cards.value = result
                    } else {
                        state.value = HomeState.Error("Error")
                    }
                }
        }
    }

    companion object {
        private const val TAG = "HomeViewModel"
    }

}

sealed class HomeState  {
    object Init : HomeState()
    data class IsLoading(val isLoading: Boolean) : HomeState()
    data class ShowToast(val message: String) : HomeState()
    data class Success(val message: String) : HomeState()
    data class Error(val msg: String) : HomeState()
}