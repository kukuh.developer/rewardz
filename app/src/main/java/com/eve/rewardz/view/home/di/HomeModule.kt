package com.eve.rewardz.view.home.di

import com.eve.rewardz.core.database.card.CardDao
import com.eve.rewardz.core.di.AppModule
import com.eve.rewardz.view.home.data.HomeRepositoryImpl
import com.eve.rewardz.view.home.domain.HomeRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module(includes = [AppModule::class])
@InstallIn(SingletonComponent::class)
class HomeModule {

    @Singleton
    @Provides
    fun provideHomeRepository(cardDao: CardDao) : HomeRepository {
        return HomeRepositoryImpl(cardDao)
    }
}