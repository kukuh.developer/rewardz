package com.eve.rewardz.view.home.domain
import com.eve.rewardz.core.database.card.Card
import kotlinx.coroutines.flow.Flow

interface HomeRepository {
    suspend fun cards(userPhone: String) : Flow<MutableList<Card>>
}