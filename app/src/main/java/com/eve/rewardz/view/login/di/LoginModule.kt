package com.eve.rewardz.view.login.di

import com.eve.rewardz.core.database.user.UserDao
import com.eve.rewardz.core.di.AppModule
import com.eve.rewardz.view.login.data.LoginRepositoryImpl
import com.eve.rewardz.view.login.domain.LoginRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module(includes = [AppModule::class])
@InstallIn(SingletonComponent::class)
class LoginModule {

    @Singleton
    @Provides
    fun provideLoginRepository(userDao: UserDao) : LoginRepository {
        return LoginRepositoryImpl(userDao)
    }
}