package com.eve.rewardz.view.card.domain
import com.eve.rewardz.core.database.card.Card
import kotlinx.coroutines.flow.Flow

interface CardRepository {
    suspend fun cards(userPhone: String) : Flow<MutableList<Card>>

    suspend fun addCard(card: Card) : Flow<Boolean>
}