package com.eve.rewardz.view.login.data

import com.eve.rewardz.core.database.user.User
import com.eve.rewardz.core.database.user.UserDao
import com.eve.rewardz.view.login.domain.LoginRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class LoginRepositoryImpl @Inject constructor(private val userDao: UserDao) : LoginRepository {
    override suspend fun getUser(phoneNumber: String): Flow<MutableList<User>> {
        return flow {
            val response = userDao.getUserByPhone(phoneNumber)
            if(response!!.size>0){
                emit(response)
            } else {
                emit(mutableListOf())
            }
        }
    }

    companion object {
        private const val TAG = "LoginRepositoryImpl"
    }
}