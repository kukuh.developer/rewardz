package com.eve.rewardz.view.card.di

import com.eve.rewardz.core.database.card.CardDao
import com.eve.rewardz.core.di.AppModule
import com.eve.rewardz.view.card.data.CardRepositoryImpl
import com.eve.rewardz.view.card.domain.CardRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module(includes = [AppModule::class])
@InstallIn(SingletonComponent::class)
class CardModule {

    @Singleton
    @Provides
    fun provideCardRepository(cardDao: CardDao) : CardRepository {
        return CardRepositoryImpl(cardDao)
    }
}