package com.eve.rewardz.view.home.domain

import com.eve.rewardz.core.database.card.Card
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class HomeUseCase @Inject constructor(private val homeRepository: HomeRepository) {
    suspend fun cards(userPhone: String): Flow<MutableList<Card>> {
        return homeRepository.cards(userPhone)
    }
}