package com.eve.rewardz.view.register.domain

import com.eve.rewardz.core.database.user.User
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class RegisterUseCase @Inject constructor(private val registerRepository: RegisterRepository) {
    suspend fun user(user: User): Flow<Boolean>{
        return registerRepository.user(user)
    }
}