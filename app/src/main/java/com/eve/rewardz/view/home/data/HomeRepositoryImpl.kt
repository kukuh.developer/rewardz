package com.eve.rewardz.view.home.data

import com.eve.rewardz.core.database.card.Card
import com.eve.rewardz.core.database.card.CardDao
import com.eve.rewardz.view.home.domain.HomeRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class HomeRepositoryImpl @Inject constructor(private val cardDao: CardDao) : HomeRepository {
    override suspend fun cards(userPhone: String): Flow<MutableList<Card>> {
        return flow {
            val response = cardDao.getCards(userPhone)
            if(response!!.size>0){
                emit(response)
            } else {
                emit(mutableListOf())
            }
        }
    }

    companion object {
        private const val TAG = "HomeRepositoryImpl"
    }
}