package com.eve.rewardz.view.login.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.eve.rewardz.core.database.user.User
import com.eve.rewardz.databinding.ActivityLoginBinding
import com.eve.rewardz.view.login.data.LoginRepositoryImpl
import com.eve.rewardz.view.login.domain.LoginUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val loginUseCase: LoginUseCase): ViewModel() {
    val state = MutableStateFlow<LoginActivityState>(LoginActivityState.Init)
    val mState: StateFlow<LoginActivityState> get() = state

    private val _users = MutableStateFlow<MutableList<User>?>(null)
    val users : StateFlow<MutableList<User>?> get() = _users

    private fun setLoading(){
        state.value = LoginActivityState.IsLoading(true)
    }

    private fun hideLoading(){
        state.value = LoginActivityState.IsLoading(false)
    }

    private fun showToast(message: String){
        state.value = LoginActivityState.ShowToast(message)
    }

    fun isInputValid(binding: ActivityLoginBinding): Boolean{
        var isPhoneValid = false

        if(binding.etPhone.text.isNotEmpty()){
            isPhoneValid = true
        }

        return isPhoneValid
    }

    fun users(phoneNumber: String){
        viewModelScope.launch {
            loginUseCase.getUser(phoneNumber)
                .onStart {
                    setLoading()
                }
                .catch { exception ->
                    hideLoading()
                    showToast(exception.message.toString())
                }
                .collect { result ->
                    hideLoading()
                    if(result.isNotEmpty()){
                        state.value = LoginActivityState.Success("Success")
                        _users.value = result
                    } else {
                        state.value = LoginActivityState.Error("Error")
                    }
                }
        }
    }

    companion object {
        private const val TAG = "LoginViewModel"
    }

}

sealed class LoginActivityState  {
    object Init : LoginActivityState()
    data class IsLoading(val isLoading: Boolean) : LoginActivityState()
    data class ShowToast(val message: String) : LoginActivityState()
    data class Success(val message: String) : LoginActivityState()
    data class Error(val msg: String) : LoginActivityState()
}