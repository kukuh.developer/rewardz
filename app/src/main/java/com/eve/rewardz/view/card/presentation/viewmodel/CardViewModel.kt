package com.eve.rewardz.view.card.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.eve.rewardz.core.database.card.Card
import com.eve.rewardz.databinding.ActivityCardAddBinding
import com.eve.rewardz.view.card.domain.CardUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CardViewModel @Inject constructor(private val cardUseCase: CardUseCase): ViewModel() {
    val state = MutableStateFlow<CardActivityState>(CardActivityState.Init)
    val mState: StateFlow<CardActivityState> get() = state

    private val _cards = MutableStateFlow<MutableList<Card>?>(null)
    val cards : StateFlow<MutableList<Card>?> get() = _cards

    private val _addCard = MutableStateFlow<Boolean?>(null)
    val addCard : StateFlow<Boolean?> get() = _addCard

    private fun setLoading(){
        state.value = CardActivityState.IsLoading(true)
    }

    private fun hideLoading(){
        state.value = CardActivityState.IsLoading(false)
    }

    private fun showToast(message: String){
        state.value = CardActivityState.ShowToast(message)
    }

    fun isInputValid(binding: ActivityCardAddBinding): Boolean{
        var isCardHolderNameValid = false
        var isCardNumberValid = false
        var isCardCvvValid = false

        if(binding.etCardHolderName.text.isNotEmpty()){
            isCardHolderNameValid = true
        }

        if(binding.etCardNumber.text.isNotEmpty()){
            isCardNumberValid = true
        }

        if(binding.etCardCvv.text.isNotEmpty()){
            isCardCvvValid = true
        }

        return isCardHolderNameValid && isCardNumberValid && isCardCvvValid
    }

    fun cards(userPhone: String){
        viewModelScope.launch {
            cardUseCase.cards(userPhone)
                .onStart {
                    setLoading()
                }
                .catch { exception ->
                    hideLoading()
                    showToast(exception.message.toString())
                }
                .collect { result ->
                    hideLoading()
                    if(result.isNotEmpty()){
                        state.value = CardActivityState.Success("Success")
                        _cards.value = result
                    } else {
                        state.value = CardActivityState.Error("Error")
                    }
                }
        }
    }
    
    fun addCard(card: Card){
        viewModelScope.launch {
            cardUseCase.addCard(card)
                .onStart {
                    setLoading()
                }
                .catch { exception ->
                    hideLoading()
                    showToast(exception.message.toString())
                }
                .collect { result ->
                    hideLoading()
                    if(result){
                        state.value = CardActivityState.Success("Success")
                        _addCard.value = result
                    } else {
                        state.value = CardActivityState.Error("Error")
                    }
                }
        }
    }

    companion object {
        private const val TAG = "CardViewModel"
    }

}

sealed class CardActivityState  {
    object Init : CardActivityState()
    data class IsLoading(val isLoading: Boolean) : CardActivityState()
    data class ShowToast(val message: String) : CardActivityState()
    data class Success(val message: String) : CardActivityState()
    data class Error(val msg: String) : CardActivityState()
}