package com.eve.rewardz.view.login.domain
import com.eve.rewardz.core.database.user.User
import kotlinx.coroutines.flow.Flow

interface LoginRepository {
    suspend fun getUser(phoneNumber: String) : Flow<MutableList<User>>
}