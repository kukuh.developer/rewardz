package com.eve.rewardz.view.card.domain

import com.eve.rewardz.core.database.card.Card
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CardUseCase @Inject constructor(private val cardRepository: CardRepository) {
    suspend fun cards(userPhone: String): Flow<MutableList<Card>> {
        return cardRepository.cards(userPhone)
    }

    suspend fun addCard(card: Card): Flow<Boolean> {
        return cardRepository.addCard(card)
    }
}