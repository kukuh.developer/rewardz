package com.eve.rewardz.view.register.di

import com.eve.rewardz.core.database.user.UserDao
import com.eve.rewardz.core.di.AppModule
import com.eve.rewardz.view.register.data.RegisterRepositoryImpl
import com.eve.rewardz.view.register.domain.RegisterRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module(includes = [AppModule::class])
@InstallIn(SingletonComponent::class)
class RegisterModule {

    @Singleton
    @Provides
    fun provideRegisterRepository(userDao: UserDao) : RegisterRepository {
        return RegisterRepositoryImpl(userDao)
    }
}