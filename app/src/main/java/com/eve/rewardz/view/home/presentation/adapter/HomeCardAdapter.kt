package com.eve.rewardz.view.home.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eve.rewardz.R
import com.eve.rewardz.core.database.card.Card
import com.eve.rewardz.databinding.ItemHomeCardBinding

class HomeCardAdapter (var context: Context,
                       var items: List<Card>
) : RecyclerView.Adapter<HomeCardAdapter.ItemViewHolder>() {

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val binding = ItemHomeCardBinding.bind(itemView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_home_card, parent, false)
        return ItemViewHolder(view)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val data = items[position]
        with(holder) {
            binding.tvCardHolderName.text = data.cardName
            binding.tvCardNumber.text = data.cardNumber
        }
    }
}