package com.eve.rewardz.view.card.presentation

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.eve.rewardz.core.database.card.Card
import com.eve.rewardz.core.utils.SharedPrefs
import com.eve.rewardz.core.utils.VerticalItemDecoration
import com.eve.rewardz.databinding.FragmentCardBinding
import com.eve.rewardz.view.card.presentation.adapter.CardAdapter
import com.eve.rewardz.view.card.presentation.viewmodel.CardActivityState
import com.eve.rewardz.view.card.presentation.viewmodel.CardViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

@AndroidEntryPoint
class CardFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var binding: FragmentCardBinding

    private lateinit var cardAdapter: CardAdapter

    private val cardViewModel: CardViewModel by viewModels()

    @Inject
    lateinit var sharedPrefs: SharedPrefs

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentCardBinding.inflate(inflater, container, false)
        observe()
        initView()
        return binding.root
    }

    private fun initView(){
        binding.btnAddCard.setOnClickListener {
            startActivity(Intent(requireContext(), CardAddActivity::class.java))
        }

        initRecyclerView()
        cards()
    }

    private fun observe() {
        observeState()
        observeCards()
    }

    fun cards(){
        cardViewModel.cards(sharedPrefs.getuserPhoneNumber())
    }

    private fun initRecyclerView() {
        cardAdapter = CardAdapter(requireContext(), listOf())
        val itemDecoration = VerticalItemDecoration(10, false)
        if(binding.rvCards.itemDecorationCount == 0){
            binding.rvCards.addItemDecoration(itemDecoration)
        }
        binding.rvCards.adapter = cardAdapter
    }

    private fun observeState() {
        cardViewModel.state.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { state -> handleState(state) }
            .launchIn(lifecycleScope)
    }

    private fun observeCards() {
        cardViewModel.cards.flowWithLifecycle(
            lifecycle,
            Lifecycle.State.STARTED
        )
            .onEach { detail ->
                detail?.let { handleCards(it) }
            }
            .launchIn(lifecycleScope)
    }

    private fun handleState(state: CardActivityState) {
        when (state) {
            is CardActivityState.Success -> {

            }
            is CardActivityState.Init -> {

            }
            is CardActivityState.ShowToast -> {

            }
            is CardActivityState.IsLoading -> {

            }
            is CardActivityState.Error -> {
                Toast.makeText(requireContext(), state.msg, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun handleCards(cards: MutableList<Card>) {
        cardAdapter.items = cards
        cardAdapter.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        cards()
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CardFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}