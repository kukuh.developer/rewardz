package com.eve.rewardz.view.register.presentation

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.eve.rewardz.core.database.user.User
import com.eve.rewardz.databinding.ActivityRegisterBinding
import com.eve.rewardz.view.register.presentation.viewmodel.RegisterActivityState
import com.eve.rewardz.view.register.presentation.viewmodel.RegisterViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding

    private val registerViewModel: RegisterViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        observe()
        initView()
    }

    fun initView(){
        binding.btnRegister.setOnClickListener {
            when(registerViewModel.isInputValid(binding))  {
                true -> {
                    val strUsername = binding.etUsername.text.toString()
                    val strEmail = binding.etEmail.text.toString()
                    val strPhone = binding.etPhone.text.toString()
                    
                    val user = User(
                        username = strUsername,
                        email = strEmail,
                        phoneNumber = strPhone
                    )
                    
                    registerViewModel.user(user)
                }
                false -> {
                    Toast.makeText(this, "Field cannot empty", Toast.LENGTH_SHORT).show()
                }
            }
        }
        binding.tvLogin.setOnClickListener {
            finish()
        }
    }

    private fun observe() {
        observeState()
        observeRegister()
    }

    private fun observeState() {
        registerViewModel.state.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { state -> handleState(state) }
            .launchIn(lifecycleScope)
    }

    private fun observeRegister() {
        registerViewModel.users.flowWithLifecycle(
            lifecycle,
            Lifecycle.State.STARTED
        )
            .onEach { detail ->
                detail?.let { handleRegister() }
            }
            .launchIn(lifecycleScope)
    }

    private fun handleState(state: RegisterActivityState) {
        when (state) {
            is RegisterActivityState.Success -> {
                Toast.makeText(this, state.message, Toast.LENGTH_SHORT).show()
            }
            is RegisterActivityState.Init -> {

            }
            is RegisterActivityState.ShowToast -> {

            }
            is RegisterActivityState.IsLoading -> {

            }
            is RegisterActivityState.Error -> {
                Toast.makeText(this, state.msg, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun handleRegister() {
        finish()
    }

    companion object {
        private const val TAG = "RegisterActivity"
    }
}