package com.eve.rewardz.view.register.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.eve.rewardz.core.database.user.User
import com.eve.rewardz.databinding.ActivityRegisterBinding
import com.eve.rewardz.view.register.domain.RegisterUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(private val registerUseCase: RegisterUseCase): ViewModel() {
    val state = MutableStateFlow<RegisterActivityState>(RegisterActivityState.Init)
    val mState: StateFlow<RegisterActivityState> get() = state

    private val _users = MutableStateFlow<Boolean?>(null)
    val users : StateFlow<Boolean?> get() = _users

    private fun setLoading(){
        state.value = RegisterActivityState.IsLoading(true)
    }

    private fun hideLoading(){
        state.value = RegisterActivityState.IsLoading(false)
    }

    private fun showToast(message: String){
        state.value = RegisterActivityState.ShowToast(message)
    }

    fun isInputValid(binding: ActivityRegisterBinding): Boolean{
        var isUsernameValid = false
        var isEmailValid = false
        var isPhoneValid = false

        if(binding.etUsername.text.isNotEmpty()){
            isUsernameValid = true
        }

        if(binding.etPhone.text.isNotEmpty()){
            isEmailValid = true
        }

        if(binding.etPhone.text.isNotEmpty()){
            isPhoneValid = true
        }

        return isUsernameValid && isPhoneValid && isEmailValid
    }

    fun user(user: User){
        viewModelScope.launch {
            registerUseCase.user(user)
                .onStart {
                    setLoading()
                }
                .catch { exception ->
                    hideLoading()
                    showToast(exception.message.toString())
                }
                .collect { result ->
                    hideLoading()
                    if(result){
                        state.value = RegisterActivityState.Success("Success")
                        _users.value = result
                    } else {
                        state.value = RegisterActivityState.Error("Error")
                    }
                }
        }
    }

}

sealed class RegisterActivityState  {
    object Init : RegisterActivityState()
    data class IsLoading(val isLoading: Boolean) : RegisterActivityState()
    data class ShowToast(val message: String) : RegisterActivityState()
    data class Success(val message: String) : RegisterActivityState()
    data class Error(val msg: String) : RegisterActivityState()
}