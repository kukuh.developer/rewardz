package com.eve.rewardz.view.login.domain

import com.eve.rewardz.core.database.user.User
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LoginUseCase @Inject constructor(private val loginRepository: LoginRepository) {
    suspend fun getUser(phoneNumber: String): Flow<MutableList<User>> {
        return loginRepository.getUser(phoneNumber)
    }
}