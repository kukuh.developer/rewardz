package com.eve.rewardz.view.register.domain
import com.eve.rewardz.core.database.user.User
import kotlinx.coroutines.flow.Flow

interface RegisterRepository {
    suspend fun user(user: User): Flow<Boolean>
}