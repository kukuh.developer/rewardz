package com.eve.rewardz.view.card.presentation

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.eve.rewardz.core.database.card.Card
import com.eve.rewardz.core.utils.SharedPrefs
import com.eve.rewardz.databinding.ActivityCardAddBinding
import com.eve.rewardz.view.card.presentation.viewmodel.CardActivityState
import com.eve.rewardz.view.card.presentation.viewmodel.CardViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class CardAddActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCardAddBinding
    
    private val cardViewModel: CardViewModel by viewModels()

    @Inject
    lateinit var sharedPrefs: SharedPrefs

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCardAddBinding.inflate(layoutInflater)
        setContentView(binding.root)

        observe()
        initView()
    }

    fun initView(){
        binding.btnAddCard.setOnClickListener { 
            when(cardViewModel.isInputValid(binding)){
                true -> {
                    val strCardHolderName = binding.etCardHolderName.text.toString()
                    val strCardNumber = binding.etCardNumber.text.toString()
                    val strCardCvv = binding.etCardCvv.text.toString()

                    val card = Card(
                        cardName = strCardHolderName,
                        cardImage = "",
                        cardNumber = strCardNumber,
                        cardCvv = strCardCvv,
                        userPhone = sharedPrefs.getuserPhoneNumber()
                    )

                    cardViewModel.addCard(card)
                }
                false -> {
                    Toast.makeText(this, "Field cannot empty", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun observe() {
        observeState()
        observeAddCard()
    }

    private fun observeState() {
        cardViewModel.state.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { state -> handleState(state) }
            .launchIn(lifecycleScope)
    }

    private fun observeAddCard() {
        cardViewModel.addCard.flowWithLifecycle(
            lifecycle,
            Lifecycle.State.STARTED
        )
            .onEach { detail ->
                detail?.let { handleAddCard() }
            }
            .launchIn(lifecycleScope)
    }

    private fun handleState(state: CardActivityState) {
        when (state) {
            is CardActivityState.Success -> {
                Toast.makeText(this, state.message, Toast.LENGTH_SHORT).show()
            }
            is CardActivityState.Init -> {

            }
            is CardActivityState.ShowToast -> {

            }
            is CardActivityState.IsLoading -> {

            }
            is CardActivityState.Error -> {
                Toast.makeText(this, state.msg, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun handleAddCard() {
        finish()
    }

    companion object {
        private const val TAG = "CardAddActivity"
    }
}