package com.eve.rewardz.core.di

import android.app.Application
import androidx.room.Room
import com.eve.rewardz.core.database.AppDatabase
import com.eve.rewardz.core.database.card.CardDao
import com.eve.rewardz.core.database.transaction.TransactionsDao
import com.eve.rewardz.core.database.user.UserDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    @Singleton
    fun provideDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(
            application,
            AppDatabase::class.java,
            AppDatabase.DATABASE_NAME
        ).build()
    }

    @Singleton
    @Provides
    fun provideUserDao(database: AppDatabase): UserDao {
        return database.userDao()
    }

    @Singleton
    @Provides
    fun provideCardDao(database: AppDatabase): CardDao {
        return database.cardDao()
    }

    @Singleton
    @Provides
    fun provideTransactionDao(database: AppDatabase): TransactionsDao {
        return database.transactionDao()
    }

//    @Provides
//    fun provideUserRepository(userDao: UserDao): UserRepository {
//        return UserRepository(userDao)
//    }
//
//    @Provides
//    fun provideMyCardRepository(myCardDao: MyCardDao): MyCardRepository {
//        return MyCardRepository(myCardDao)
//    }
}