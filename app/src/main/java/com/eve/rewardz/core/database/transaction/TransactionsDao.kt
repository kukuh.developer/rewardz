package com.eve.rewardz.core.database.transaction

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface TransactionsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTransaction(transaction: Transactions)

    @Query("SELECT * FROM Transactions WHERE transactionDate = :transactionDate")
    suspend fun getTransactions(transactionDate: String): MutableList<Transactions>?
}