package com.eve.rewardz.core.database.card

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CardDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCard(myCard: Card)

    @Query("SELECT * FROM Card WHERE userPhone = :userPhone")
    suspend fun getCards(userPhone: String): MutableList<Card>?
}