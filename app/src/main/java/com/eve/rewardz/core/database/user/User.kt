package com.eve.rewardz.core.database.user

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "User")
data class User(
    @PrimaryKey
    val phoneNumber: String,
    val username: String,
    val email: String
)
