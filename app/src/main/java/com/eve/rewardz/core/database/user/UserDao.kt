package com.eve.rewardz.core.database.user

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(user: User)

    @Query("SELECT * FROM User WHERE phoneNumber = :phoneNumber")
    suspend fun getUserByPhone(phoneNumber: String): MutableList<User>?
}