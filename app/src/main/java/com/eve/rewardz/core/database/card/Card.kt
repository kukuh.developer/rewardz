package com.eve.rewardz.core.database.card

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Card")
data class Card(
    @PrimaryKey
    val cardNumber: String,
    val cardName: String,
    val cardImage: String,
    val cardCvv: String,
    val userPhone: String
)