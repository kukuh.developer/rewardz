package com.eve.rewardz.core.database.transaction

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Transactions")
data class Transactions(
    @PrimaryKey(autoGenerate = true)
    val transaction_id: Long = 0,
    val transactionName: String,
    val transactionAmmount: Long,
    val transactionDesc: String,
    val transactionDate: String
)