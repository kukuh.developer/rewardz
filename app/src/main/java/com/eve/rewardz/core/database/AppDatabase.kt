package com.eve.rewardz.core.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.eve.rewardz.core.database.card.Card
import com.eve.rewardz.core.database.card.CardDao
import com.eve.rewardz.core.database.transaction.TransactionsDao
import com.eve.rewardz.core.database.transaction.Transactions
import com.eve.rewardz.core.database.user.User
import com.eve.rewardz.core.database.user.UserDao

@Database(entities = [User::class, Card::class, Transactions::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun cardDao(): CardDao
    abstract fun transactionDao(): TransactionsDao

    companion object {
        val DATABASE_NAME = "app_database"
    }
}